package test

import (
	"testing"

	loaderBlob "gitlab.com/republico/library/go/azure/pkg/loader_blob"
)

func TestExecuteBlockFileLocal(t *testing.T) {
	loaderBlob.Config.Account = ""
	loaderBlob.Config.Key = ""
	loaderBlob.Config.Container = "siconv"
	loaderBlob.Config.Blob = "consorcios.json"

	operation := "file"
	data := "C:/consorcios.json"

	_, _, err := loaderBlob.Execute(operation, data)
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestExecuteBlockFileOnline(t *testing.T) {
	loaderBlob.Config.Account = ""
	loaderBlob.Config.Key = ""
	loaderBlob.Config.Container = "siconv"
	loaderBlob.Config.Blob = "consorcios.json"

	operation := "file"
	data := "http://portal.convenios.gov.br/images/docs/CGSIS/csv/siconv_consorcios.csv.zip"

	_, _, err := loaderBlob.Execute(operation, data)
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestExecuteBlockText(t *testing.T) {
	loaderBlob.Config.Account = ""
	loaderBlob.Config.Key = ""
	loaderBlob.Config.Container = "siconv"
	loaderBlob.Config.Blob = "consorcios.json"

	operation := "append"
	data := "{\"id\":\"1\",\"name\":\"Test\"}"

	_, _, err := loaderBlob.Execute(operation, data)
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestExecuteCreateAppend(t *testing.T) {
	loaderBlob.Config.Account = ""
	loaderBlob.Config.Key = ""
	loaderBlob.Config.Container = "siconv"
	loaderBlob.Config.Blob = "consorcios.json"

	operation := "createappend"
	data := ""

	_, _, err := loaderBlob.Execute(operation, data)
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}

func TestExecuteAppendText(t *testing.T) {
	loaderBlob.Config.Account = ""
	loaderBlob.Config.Key = ""
	loaderBlob.Config.Container = "siconv"
	loaderBlob.Config.Blob = "consorcios.json"

	operation := "append"
	data := "{\"id\":\"1\",\"name\":\"Test\"}"

	_, _, err := loaderBlob.Execute(operation, data)
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}