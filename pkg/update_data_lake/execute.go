package update_data_lake

import (
	"errors"
	"strconv"
	"strings"
	"time"

	"gitlab.com/republico/library/go/azure/pkg/auth"
	utilDate "gitlab.com/republico/library/go/util/pkg/date"
)

func Execute(filesystem string, path string, data string) (int, string, error) {
	date := strings.Replace(time.Now().UTC().Format(time.RFC1123), "UTC", "GMT", 3)

	urlBase := "https://" + Config.Account + ".dfs.core.windows.net"
	contentType := "text/plain; charset=UTF-8"
	version := "2018-11-09"
	method := "PUT"
	pathType := "Append Data"
	pathUrl := "/" + filesystem + "/" + getSuffixPath(date) + path
	queryUrl := "?action=append"
	url := urlBase + pathUrl + queryUrl
	contentLength := strconv.Itoa(len(data))

	authorization, err := auth.GetAuthorization(Config.Account, Config.Key, pathUrl, queryUrl, method, pathType, date, version, contentLength, contentType)
	if err != nil {
		return 0, "", err
	}

	request, err := createRequest(method, url, data)
	if err != nil {
		return 0, "", err
	}

	request = configHeader(request, authorization, contentLength, contentType, pathType, date, version)
	code, message, err := run(request)
	if err != nil {
		return 0, "", err
	}

	if code < 200 || code > 299 {
		if err != nil {
			return code, "", errors.New(message)
		}
	}

	return code, message, nil
}

func getSuffixPath(date string) (string) {
	dateSplit := strings.Split(date, " ")
	hourSplit := strings.Split(dateSplit[4], ":")

	return dateSplit[3] + "_" + 
		utilDate.ConvertMounthLettersInNumber(dateSplit[2]) + "_" + 
		dateSplit[1] + "_" + 
		hourSplit[0] + "_" + 
		hourSplit[1] + "_" + 
		hourSplit[2] + "__"
}