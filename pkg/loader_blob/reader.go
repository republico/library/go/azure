package loader_blob

import (
	"io/ioutil"
	"net/http"
)

func downloadFile(url string) (string, error) {
    resp, err := http.Get(url)
    if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	bodyResp, err := ioutil.ReadAll(resp.Body)
    if err != nil {
		return "", err
	}
	data := string(bodyResp)

	return data, nil
}

func readFile(path string) (string, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}

	data := string(file[:])
	return data, nil
}