package loader_blob

import (
	"errors"
	"strings"

	utilDate "gitlab.com/republico/library/go/util/pkg/date"
)

func mountBody(operation string, data string) (string, error) {
	reqBody := ""

	if (operation == "append" || operation == "block") {
		reqBody = data
	} else if operation == "download" {
		req, err := downloadFile(data)
		if err != nil {
			return "", err
		} else{
			reqBody = req
		}
	} else if operation == "file" {
		req, err := readFile(data)
		if err != nil {
			return "", err
		} else{
			reqBody = req
		}
	} else {
		err := errors.New("Tipo de dado inválido.")
		if err != nil {
			return "", err
		}
	}

	return reqBody, nil
}

func generateBlobName(blobName string) string {
	dateSplit := strings.Split(date, " ")
	hourSplit := strings.Split(dateSplit[4], ":")

	result := dateSplit[3] + "_" + 
		utilDate.ConvertMounthLettersInNumber(dateSplit[2]) + "_" + 
		dateSplit[1] + "_" + 
		hourSplit[0] + "_" + 
		hourSplit[1] + "_" + 
		hourSplit[2] + "__" + 
		blobName

	return result
}

func adjustQueryUrl(queryUrl string) string {
	queryUrlAdjusted := queryUrl
	
	if queryUrlAdjusted != "" {
		queryUrlAdjusted = queryUrlAdjusted[1:]
		queryUrlAdjusted = strings.Replace(queryUrlAdjusted, "=", ":", -1)
		queryUrlAdjusted = strings.Replace(queryUrlAdjusted, "&", "\n", -1)
	}

	return queryUrlAdjusted
}