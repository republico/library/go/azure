package loader_blob

import (
	"errors"
	"strings"
	"time"
)

var date string

func Execute(container string, blob string, data string, operation string) (int, string, error) {
	date = strings.Replace(time.Now().UTC().Format(time.RFC1123), "UTC", "GMT", 3)

	if (operation == "append" || operation == "block") && data == "" {
		err := errors.New("Necessário a passagem do dado a ser carregado.")
		if err != nil {
			return 0, "", err
		}
	}

	blobAdjusted := blob
	if operation != "text" && (blob == "." || blob == "_") {
		blobAdjusted = data[strings.LastIndex(data, "/") + 1:]
	}

	body, err := mountBody(operation, data)
	if err != nil {
		return 0, "", err
	}

	code := 400
	message := "Ocorreu um erro."

	code, message, err = getMetadataContainer(container, blobAdjusted)
	if err != nil {
		return code, "", err
	}

	if code == 404 {
		code, message, err = createContainer(container, blobAdjusted)
		if code != 201 {
			err := errors.New(message)
			if err != nil {
				return code, "", err
			}
		}
	}

	code, message, err = getMetadataBlob(container, blobAdjusted)
	if err != nil {
		return code, "", err
	}

	if operation == "block" {
		if code == 404 {	
			code, message, err = blockBlob(container, blobAdjusted, body)
			if err != nil {
				return code, "", err
			}

			if code < 200 || code > 299 {
				if err != nil {
					return code, "", errors.New(message)
				}
			}
		} else {
			if err != nil {
				return code, "", errors.New("Arquivo já existe")
			}
		}
	} else if operation == "append" {
		if code == 404 {
			code, message, err = createBlobAppend(container, blobAdjusted)
			if err != nil {
				return code, "", err
			}

			if code != 201 {
				err := errors.New(message)
				if err != nil {
					return code, "", err
				}
			} else {
				code, message, err = appendBlob(container, blobAdjusted, body)
				if err != nil {
					return code, "", err
				}

				if code < 200 || code > 299 {
					err := errors.New(message)
					if err != nil {
						return code, "", err
					}
				}
			}
		}
	} else {
		if code == 404 {	
			code, message, err = appendBlob(container, blobAdjusted, body)
			if err != nil {
				return code, "", err
			}

			if code < 200 || code > 299 {
				if err != nil {
					return code, "", errors.New(message)
				}
			}
		} else {
			if err != nil {
				return code, "", errors.New("Arquivo já existe")
			}
		}
	}

	return code, message, nil
}