package loader_blob

import (
	"strconv"

	"gitlab.com/republico/library/go/azure/pkg/auth"
)

func load(container string, blob string, method string, blobType string, body string) (int, string, error) {
	account := Config.Account
	key := Config.Key
	urlBase := "https://" + account + ".blob.core.windows.net"
	contentType := "text/plain; charset=UTF-8"
	version := "2018-03-28"

	blobAdjusted := ""
	if blob != "" {
		blobAdjusted = "/" + blob
	}

	pathUrl := "/" + container + blobAdjusted
	queryUrl := generateQueryUrl(blobType)
	url := urlBase + pathUrl + queryUrl

	contentLength := strconv.Itoa(len(body))
	authorization, err := auth.GetAuthorization(account, key, pathUrl, queryUrl, method, blobType, date, version, contentLength, contentType)
	if err != nil {
		return 0, "", err
	}

	request, err := createRequest(method, url, body)
	if err != nil {
		return 0, "", err
	}

	request = configHeader(request, authorization, contentLength, contentType, blobType, date, version)
	code, message, err := run(request)

	if err != nil {
		return 0, "", err
	}

	return code, message, nil
}

func generateQueryUrl(blobType string) string {
	queryUrl := ""

	switch blobType {
    case "createcontainer":
        queryUrl += "?restype=container"
    case "append":
        queryUrl += "?comp=appendblock"
    case "metadatacontainer":
        queryUrl += "?comp=metadata&restype=container"
    case "metadatablob":
        queryUrl += "?comp=metadata"
	}

	return queryUrl
}