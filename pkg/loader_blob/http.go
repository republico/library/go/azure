package loader_blob

import (
	"io/ioutil"
	"net/http"
	"strings"
)

func createRequest(method string, url string, body string) (*http.Request, error) {
	return http.NewRequest(method, url, strings.NewReader(body))
}

func configHeader(req *http.Request, authorization string, contentLength string, contentType string, blobType string, date string, version string) *http.Request {
	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Length", contentLength)
	req.Header.Set("Content-Type", contentType)
	req.Header.Set("x-ms-blob-type", blobType)
	req.Header.Set("x-ms-date", date)
	req.Header.Set("x-ms-version", version)

	return req
}

func run(req *http.Request) (int, string, error) {
	client := &http.Client{}

	resp, err := client.Do(req)
    if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
    if err != nil {
		return 0, "", err
	}

	respCode := resp.StatusCode
	respBodyString := string(respBody)

	return respCode, respBodyString, nil
}