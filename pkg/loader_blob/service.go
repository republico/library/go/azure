package loader_blob

func getMetadataContainer(container string, blob string) (int, string, error) {
	return loadService(container, blob, "GET", "metadatacontainer", "")
}

func getMetadataBlob(container string, blob string) (int, string, error) {
	return loadService(container, blob, "GET", "metadatablob", "")
}

func createContainer(container string, blob string) (int, string, error) {
	return loadService(container, blob, "PUT", "createcontainer", "")
}

func createBlobAppend(container string, blob string) (int, string, error) {
	return loadService(container, blob, "PUT", "AppendBlob", "")
}

func appendBlob(container string, blob string, body string) (int, string, error) {
	return loadService(container, blob, "PUT", "append", body)
}

func blockBlob(container string, blob string, body string) (int, string, error) {
	return loadService(container, blob, "PUT", "BlockBlob", body)
}

func loadService(container string, blob string, method string, blobType string, body string) (int, string, error) {
	code, message, err := load(container, blob, method, blobType, body)
	if err != nil {
		return code, "", err
	}

	return code, message, err
}