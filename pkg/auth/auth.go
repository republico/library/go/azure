package auth

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
)

func GetAuthorization(account string, key string, pathUrl string, queryUrl string, method string, blobType string, date string, version string, contentLength string, contentType string) (string, error) {
	signString := generateSignatureString(account, pathUrl, queryUrl, method, blobType, date, version, contentLength, contentType)

	sign, err := generateMac(key, signString)
	if err != nil {
		return "", err
	}

	authorization := generateAuthorization(account, sign)

	return authorization, nil
}

func generateSignatureString(account string, pathUrl string, queryUrl string, method string, blobType string, date string, version string, contentLength string, contentType string) string {
	signString := method + "\n"
	signString += "\n" /*content encoding*/
	signString += "\n" /*content language*/
	signString += generateContentLengthSign(blobType, queryUrl, contentLength) /*content length*/
	signString += "\n" /*content md5*/
	signString += contentType + "\n" /*content type*/
	signString += "\n" /*date*/
	signString += "\n" /*if modified since*/
	signString += "\n" /*if match*/
	signString += "\n" /*if none match*/
	signString += "\n" /*if unmodified since*/
	signString += "\n" /*range*/
	signString += "x-ms-blob-type:" + blobType + "\n"
	signString += "x-ms-date:" + date + "\n"
	signString += "x-ms-version:" + version + "\n"
	signString += "/" + account + pathUrl

	if queryUrl != "" {
		signString += "\n" + queryUrl
	}

	return signString
}

func generateMac(key string, signatureString string) (string, error) {
	keyDecode, err := base64.StdEncoding.DecodeString(key)
    if err != nil {
		return "", err
	}

	hash := hmac.New(sha256.New, []byte(keyDecode))
	hash.Write([]byte(signatureString))
	signature := base64.StdEncoding.EncodeToString(hash.Sum(nil))
	return signature, nil
}

func generateAuthorization(account string, signature string) string {
	authorization := "SharedKey " + account + ":" + signature
	return authorization
}

func generateContentLengthSign(blobType string, queryUrl string, contentLength string) string {
	signString := contentLength + "\n"

	switch blobType {
	case "metadatacontainer", "metadatablob":
		signString = "\n"
	case "AppendBlob":
		if queryUrl == "" {
			signString = "\n"
		}
	}

	return signString
}